import { Controller } from '@nestjs/common'
import { Observable, Subject } from 'rxjs'
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices'
import { HeroById } from '../hero/interfaces/hero-by-id.interface'
import { Hero } from '../hero/interfaces/hero.interface'
import { SubscriberService } from './subscriber.service'

@Controller()
export class SubscriberController {
  private readonly items: Hero[] = [
    { id: 1, name: 'John' },
    { id: 2, name: 'Doe' },
  ]

  constructor(private readonly subscriberService: SubscriberService) {}

  @GrpcMethod('HeroService')
  findOne(data: HeroById): Hero {
    return this.items.find(({ id }) => id === data.id)
  }

  @GrpcStreamMethod('HeroService')
  findMany(data$: Observable<HeroById>): Observable<Hero> {
    const hero$ = new Subject<Hero>()

    const onNext = (heroById: HeroById) => {
      const item = this.items.find(({ id }) => id === heroById.id)
      hero$.next(item)
    }
    const onComplete = () => hero$.complete()
    data$.subscribe({
      next: onNext,
      complete: onComplete,
    })

    return hero$.asObservable()
  }
}
