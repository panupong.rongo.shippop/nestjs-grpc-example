import { Module } from '@nestjs/common'
import { ClientsModule } from '@nestjs/microservices'
import { SubscriberService } from './subscriber.service'
import { SubscriberController } from './subscriber.controller'
import { grpcClientOptions } from '../grpc-client.options'

@Module({
  controllers: [SubscriberController],
  providers: [SubscriberService],
  imports: [
    ClientsModule.register([
      {
        name: 'HERO_PACKAGE',
        ...grpcClientOptions,
      },
    ]),
  ],
})
export class SubscriberModule {}
