import { Module } from '@nestjs/common'
import { ClientsModule } from '@nestjs/microservices'

import { HeroService } from './hero.service'
import { HeroController } from './hero.controller'
import { grpcClientOptions } from '../grpc-client.options'

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'HERO_PACKAGE',
        ...grpcClientOptions,
      },
    ]),
  ],
  controllers: [HeroController],
  providers: [HeroService],
})
export class HeroModule {}
