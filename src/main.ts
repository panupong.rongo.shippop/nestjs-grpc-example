import { NestFactory } from '@nestjs/core'
import { ConfigService } from '@nestjs/config'
import { MicroserviceOptions } from '@nestjs/microservices'

// import { Logger } from 'nestjs-pino'
import { AppModule } from './app.module'
import { grpcClientOptions } from './grpc-client.options'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true })

  // app.useLogger(app.get(Logger))

  const configService = app.get(ConfigService)
  const port = configService.get<number>('port')
  const preBuildEnv = configService.get<string>('preBuildEnv')

  console.info(`app running @ port: `, port)
  console.info(`test binding data from prebuild: `, preBuildEnv)

  app.connectMicroservice<MicroserviceOptions>(grpcClientOptions)
  await app.startAllMicroservices()

  await app.listen(port || 3000)
}
bootstrap()
