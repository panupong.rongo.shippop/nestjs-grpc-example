import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
// import { LoggerModule } from 'nestjs-pino'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { HeroModule } from './hero/hero.module'
import configuration from './config/configuration'
import { SubscriberModule } from './subscriber/subscriber.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'], // ตั้งค่าตัวแปร env file path ที่จะอ่าน
      load: [configuration],
      isGlobal: true,
    }),
    HeroModule,
    SubscriberModule,
    // LoggerModule.forRoot({
    //   // pinoHttp: {
    //   //   transport: {
    //   //     target: 'pino-pretty',
    //   //     options: {
    //   //       colorize: true,
    //   //       messageFormat:
    //   //         '{levelLabel} - {pid} - url:{request.url} - {context}',
    //   //     },
    //   //   },
    //   // },
    // }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
